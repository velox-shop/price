package shop.velox.price.service.impl;

import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;
import static shop.velox.price.dao.PriceSpecification.hasStatus;
import static shop.velox.price.dao.PriceSpecification.isActive;
import static shop.velox.price.dao.PriceSpecification.isCatalogCodeMissingOrEqualTo;
import static shop.velox.price.dao.PriceSpecification.isCurrencyEqual;
import static shop.velox.price.dao.PriceSpecification.isCurrent;
import static shop.velox.price.dao.PriceSpecification.isMinQuantityLessThanOrEqualTo;
import static shop.velox.price.dao.PriceSpecification.isProductCodeIn;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.price.api.dto.PriceStatus;
import shop.velox.price.api.dto.ProductPriceDto;
import shop.velox.price.dao.CurrencyRepository;
import shop.velox.price.dao.PriceRepository;
import shop.velox.price.model.CurrencyEntity;
import shop.velox.price.model.PriceEntity;
import shop.velox.price.model.PriceEntity.Fields;
import shop.velox.price.service.PriceService;

@Service
@RequiredArgsConstructor
public class PriceServiceImpl implements PriceService {

  private static final Logger LOG = LoggerFactory.getLogger(PriceServiceImpl.class);

  private final PriceRepository priceRepository;
  private final CurrencyRepository currencyRepository;

  @Override
  public PriceEntity createPrice(final PriceEntity price) {
    CurrencyEntity currency = getCurrency(price.getCurrency());
    price.setCurrency(currency);

    try {
      if (price.getValidFrom().isBefore(price.getValidTo())) {
        return priceRepository.save(price);
      } else {
        throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
            "validTo time cannot be after validFrom");
      }

    } catch (DataIntegrityViolationException dataIntegrityViolationException) {
      LOG.info("dataIntegrityViolationException: {}", dataIntegrityViolationException.getMessage());
      // It works on MySql, but does not work on Derby.
      if (dataIntegrityViolationException.getMessage()
          .contains(PriceEntity.COMPOSITE_CONSTRAINT_NAME)) {
        LOG.info("cannot create price {}, it already exists", price);
        PriceEntity alreadyExistingPrice = priceRepository.
            findOneByCatalogCodeAndProductCodeAndValidFromAndValidToAndMinQuantityAndUnitPriceAndCurrency(
                price.getCatalogCode(), price.getProductCode(), price.getValidFrom(),
                price.getValidTo(), price.getMinQuantity(), price.getUnitPrice(), currency)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                "price not found", dataIntegrityViolationException) {
            });

        price.setPk(alreadyExistingPrice.getPk());
        price.setId(alreadyExistingPrice.getId());
        return priceRepository.save(price);

      } else {
        LOG.error("when creating price: {} : {}", price,
            dataIntegrityViolationException.getMostSpecificCause().getMessage());
        throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
            dataIntegrityViolationException.getMostSpecificCause().getMessage());
      }
    }
  }



  @Override
  public PriceEntity update(String id, Boolean active) {

    LOG.info("Update {}, {}", id, active);

    PriceEntity existingPrice = priceRepository.findOneById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    existingPrice.setActive(active);

    return priceRepository.save(existingPrice);
  }

  @Override
  public Optional<PriceEntity> findById(String id) {
    return priceRepository.findOneById(id);
  }

  @Override
  public Page<PriceEntity> findAll(Pageable pageable, PriceStatus filter, String currencyId,
      String currencyIsoCode) {
    return priceRepository.findAll(
        hasStatus(filter)
            .and(isCurrencyEqual(currencyId, currencyIsoCode)),
        pageable);
  }

  @Override
  public Page<PriceEntity> findAllByProductCodes(Pageable pageable, List<String> productCodes,
      PriceStatus filter, String currencyId, String currencyIsoCode) {
    LOG.info("findAllByProductCodes with productCodes: {}, filters: {}, currencyId: {}",
        String.join(", ", emptyIfNull(productCodes)), filter, currencyId);

    return priceRepository.findAll(
        isProductCodeIn(productCodes)
            .and(hasStatus(filter))
            .and(isCurrencyEqual(currencyId, currencyIsoCode)),
        pageable);
  }

  @Override
  public Optional<PriceEntity> getAppliedPrice(String productCode, @Nullable String catalogCode,
      BigDecimal quantity, String currencyIsoCode) {
    Page<PriceEntity> page = priceRepository
        .findAll(isProductCodeIn(ListUtils.emptyIfNull(List.of(productCode)))
                .and(isCatalogCodeMissingOrEqualTo(catalogCode))
                .and(isActive())
                .and(isCurrent())
                .and(isMinQuantityLessThanOrEqualTo(quantity))
                .and(isCurrencyEqual(null, currencyIsoCode)),
            PageRequest.of(0, 1, Sort.by(Direction.ASC, Fields.unitPrice)));
    LOG.debug("getAppliedPrice with productCode: {}, quantity: {} finds {} results", productCode,
        quantity, page.getTotalElements());
    if (CollectionUtils.isNotEmpty(page.getContent())) {
      return Optional.ofNullable(page.getContent().get(0));
    } else {
      return Optional.empty();
    }
  }

  @Override
  public ProductPriceDto applyPrice(PriceEntity priceEntity, BigDecimal quantity) {
    return new ProductPriceDto(priceEntity.getCatalogCode(), priceEntity.getProductCode(), quantity,
        quantity.multiply(priceEntity.getUnitPrice()).setScale(2, RoundingMode.DOWN),
        priceEntity.getUnitPrice(), priceEntity.getCurrency().getIsoCode());
  }

  private CurrencyEntity getCurrency(CurrencyEntity currencyEntityExample) {
    if (StringUtils.isNotBlank(currencyEntityExample.getId())) {
      String currencyId = currencyEntityExample.getId();
      return currencyRepository.findOneById(currencyId)
          .orElseThrow(
              () -> new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
                  String.format("Could not find currency with id: %s", currencyId)));
    }

    if (StringUtils.isNotBlank(currencyEntityExample.getIsoCode())) {
      String currencyIsoCode = currencyEntityExample.getIsoCode();
      return currencyRepository.findOneByIsoCode(currencyIsoCode)
          .orElseThrow(
              () -> new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
                  String.format("Could not find currency with isoCode: %s", currencyIsoCode)));
    }

    LOG.error("Cannot search for currency using {}", currencyEntityExample);
    throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Could not find currency");

  }

}
