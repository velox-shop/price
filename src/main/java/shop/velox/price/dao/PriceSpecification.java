package shop.velox.price.dao;

import static java.time.LocalDateTime.now;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static shop.velox.price.model.PriceEntity.CATALOG_CODE_DEFAULT;

import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.domain.Specification;
import shop.velox.price.api.dto.PriceStatus;
import shop.velox.price.model.PriceEntity;
import shop.velox.price.model.PriceEntity.Fields;

public class PriceSpecification {

  public static Specification<PriceEntity> isActive() {
    return (root, query, builder) -> root.get(Fields.active).in(true);
  }

  public static Specification<PriceEntity> isCurrent() {
    return (root, query, builder) -> {
      Predicate validFrom = builder.lessThan(root.get(Fields.validFrom), LocalDateTime.now());
      Predicate validTo = builder.greaterThan(root.get(Fields.validTo), LocalDateTime.now());
      return builder.and(validFrom, validTo);
    };
  }

  public static Specification<PriceEntity> isMinQuantityLessThanOrEqualTo(BigDecimal quantity) {
    return (root, query, builder) -> builder.lessThanOrEqualTo(root.get(Fields.minQuantity),
        quantity);
  }

  public static Specification<PriceEntity> isCatalogCodeMissingOrEqualTo(String catalogCode) {
    return (root, query, builder) -> {
      Predicate isMissing = builder.equal(root.get(Fields.catalogCode), CATALOG_CODE_DEFAULT);
      Predicate isEqual = builder.equal(root.get(Fields.catalogCode), catalogCode);
      return builder.or(isMissing, isEqual);
    };
  }


  public static Specification<PriceEntity> isProductCodeIn(List<String> productCodes) {
    return (root, query, builder) -> {
      if (isNotEmpty(productCodes)) {
        return root.get(Fields.productCode).in(productCodes);
      } else {
        return null;
      }
    };
  }

  public static Specification<PriceEntity> isCurrencyEqual(String currencyId,
      String currencyIsoCode) {
    return (root, query, builder) -> {
      if (isNotBlank(currencyId)) {
        Join<Object, Object> priceToCurrencyJoin = root.join("currency");
        Path<Object> idPath = priceToCurrencyJoin.get("id");
        return builder.equal(idPath, currencyId);
      }
      if (isNotBlank(currencyIsoCode)) {
        Join<Object, Object> priceToCurrencyJoin = root.join("currency");
        Path<Object> idPath = priceToCurrencyJoin.get("isoCode");
        return builder.equal(idPath, currencyIsoCode);
      }
      return null;

    };
  }

  public static Specification<PriceEntity> hasStatus(PriceStatus priceStatus) {
    return (root, query, builder) -> {
      if (PriceStatus.ACTIVE.equals(priceStatus)) {
        return builder.and(
            builder.isTrue(root.get("active")),
            builder.lessThan(root.get("validFrom"), now()),
            builder.greaterThan(root.get("validTo"), now())
        );
      } else if (PriceStatus.INACTIVE.equals(priceStatus)) {
        return builder.or(
            builder.isFalse(root.get("active")),
            builder.lessThan(root.get("validTo"), now())
        );
      } else if (PriceStatus.UPCOMING.equals(priceStatus)) {
        return builder.and(
            builder.isTrue(root.get("active")),
            builder.greaterThan(root.get("validFrom"), now())
        );
      } else {
        return null;
      }
    };
  }

}
