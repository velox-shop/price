package shop.velox.price.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;
import org.apache.commons.lang3.StringUtils;
import shop.velox.commons.model.AbstractEntity;
import shop.velox.price.model.PriceEntity.Fields;

@Entity
@Table(uniqueConstraints = {
    // This index has to contain all fields, except "active"
    @UniqueConstraint(name = PriceEntity.COMPOSITE_CONSTRAINT_NAME,
        columnNames = {Fields.catalogCode, Fields.productCode, Fields.validFrom, Fields.validTo,
            Fields.minQuantity, Fields.unitPrice, PriceEntity.CURRENCY_FIELD_NAME}),
})
@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = true)
@Jacksonized
@NoArgsConstructor
@FieldNameConstants
public class PriceEntity extends AbstractEntity {

  public static final String COMPOSITE_CONSTRAINT_NAME = "Composite_Constraint";

  public static final String CATALOG_CODE_DEFAULT = "*";
  public static final LocalDateTime VALID_FROM_DEFAULT = LocalDateTime.of(2000, 1, 1, 0, 0);
  public static final LocalDateTime VALID_TO_DEFAULT = LocalDateTime.of(2100, 1, 1, 0, 0);
  public static final BigDecimal MIN_QUANTITY_DEFAULT = BigDecimal.ZERO;

  public static final String CURRENCY_FIELD_NAME = "currency_isoCode";


  @PrePersist
  private void prePersistFunction() {
    if (StringUtils.isEmpty(id)) {
      id = UUID.randomUUID().toString();
    }
  }

  @Column(unique = true, nullable = false)
  private String id;

  @Column(length = 50)
  @Builder.Default
  private String catalogCode = CATALOG_CODE_DEFAULT;

  @Column(nullable = false, length = 50)
  private String productCode;

  @Column(nullable = false)
  @Builder.Default
  private LocalDateTime validFrom = VALID_FROM_DEFAULT;

  @Column(nullable = false)
  @Builder.Default
  private LocalDateTime validTo = VALID_TO_DEFAULT;

  @Column(nullable = false)
  @Builder.Default
  private BigDecimal minQuantity = MIN_QUANTITY_DEFAULT;

  @Column(nullable = false)
  @NotNull
  private BigDecimal unitPrice;

  @Column(nullable = false)
  @Builder.Default
  private boolean active = true;

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumn(name = CURRENCY_FIELD_NAME, referencedColumnName = CurrencyEntity.Fields.isoCode)
  @NotNull
  private CurrencyEntity currency;

}
