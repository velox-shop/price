package shop.velox.price.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotEmpty;
import java.util.UUID;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;
import org.apache.commons.lang3.StringUtils;
import shop.velox.commons.model.AbstractEntity;

@Entity
@Table(uniqueConstraints = {
    @UniqueConstraint(name = CurrencyEntity.COMPOSITE_CONSTRAINT_NAME, columnNames = {"isoCode"}),
})
@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = true)
@Jacksonized
@NoArgsConstructor
@FieldNameConstants
public class CurrencyEntity extends AbstractEntity {

  public static final String COMPOSITE_CONSTRAINT_NAME = "Currency_Unique_Constraint";

  @PrePersist
  private void prePersistFunction() {
    if (StringUtils.isEmpty(id)) {
      id = UUID.randomUUID().toString();
    }
  }

  @Column(unique = true, nullable = false)
  private String id;

  @Column(nullable = false, length = 3)
  @NotEmpty
  private String isoCode;

  @Column(nullable = false)
  @NotEmpty
  private String name;

  @Column(nullable = false)
  @NotEmpty
  private String symbol;
}
