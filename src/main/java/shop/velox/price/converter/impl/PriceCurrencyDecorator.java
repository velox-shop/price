package shop.velox.price.converter.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import shop.velox.price.dao.CurrencyRepository;
import shop.velox.price.model.CurrencyEntity;

@Component
@RequiredArgsConstructor
@Slf4j
public class PriceCurrencyDecorator {

  private final CurrencyRepository currencyRepository;

  public CurrencyEntity map(String isoCode){
    return currencyRepository.findOneByIsoCode(isoCode)
        .orElseThrow(() -> new RuntimeException(isoCode + " not found"));
  }

}
