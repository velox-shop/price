package shop.velox.price.converter.impl;

import org.mapstruct.Mapper;
import shop.velox.commons.converter.Converter;
import shop.velox.price.api.dto.CurrencyDto;
import shop.velox.price.model.CurrencyEntity;

@Mapper
public interface CurrencyConverter extends Converter<CurrencyEntity, CurrencyDto> {

}
