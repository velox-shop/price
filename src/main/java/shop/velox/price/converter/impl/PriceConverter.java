package shop.velox.price.converter.impl;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValueMappingStrategy;
import shop.velox.commons.converter.Converter;
import shop.velox.price.api.dto.PriceDto;
import shop.velox.price.model.PriceEntity;

@Mapper(uses = CurrencyConverter.class,
    nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface PriceConverter extends Converter<PriceEntity, PriceDto> {

  @Override
  @Mappings({
      @Mapping(target = "catalogCode", expression = """
          java(priceEntity.getCatalogCode()
            .equals(shop.velox.price.model.PriceEntity.CATALOG_CODE_DEFAULT) ?
            null : priceEntity.getCatalogCode())
          """)
  })
  PriceDto convertEntityToDto(PriceEntity priceEntity);
}
