package shop.velox.price.converter.impl;

import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import shop.velox.price.api.dto.CreatePriceDto;
import shop.velox.price.model.PriceEntity;

@Mapper(uses = PriceCurrencyDecorator.class,
    builder = @Builder(disableBuilder = true)
)
public interface CreatePriceConverter {

  @Mappings({
      @Mapping(target = "pk", ignore = true),
      @Mapping(target = "createTime", ignore = true),
      @Mapping(target = "modifiedTime", ignore = true),
      @Mapping(target = "id", ignore = true),
      @Mapping(target = "currency", source = "currencyIsoCode")
  })
  PriceEntity convert(CreatePriceDto createPriceDto);

}
