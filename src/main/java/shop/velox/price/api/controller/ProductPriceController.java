package shop.velox.price.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.math.BigDecimal;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import shop.velox.price.api.dto.ProductPriceDto;
import shop.velox.price.api.dto.ProductPriceListDto;
import shop.velox.price.api.dto.ProductPriceRequestListDto;


@Tag(name = "Product Prices", description = "the API to get Prices of specific products")
@RequestMapping(value = "/products/prices", produces = MediaType.APPLICATION_JSON_VALUE)
public interface ProductPriceController {

  @Operation(summary = "gets applied price for a list of products at the specified quantity")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200",
          description = "returns the price for all the products in the request",
          content = @Content(schema = @Schema(implementation = ProductPriceListDto.class))),
      @ApiResponse(responseCode = "206",
          description = "Not all the products in the request have a price. Returning only the ones which have it.",
          content = @Content(schema = @Schema(implementation = ProductPriceListDto.class))),
      @ApiResponse(responseCode = "422",
          description = "Mandatory parameters are missing. (e.g. product code)",
          content = @Content(schema = @Schema())),
  })
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<ProductPriceListDto> getPrices(
      @RequestBody ProductPriceRequestListDto productPriceRequestListDto);

  @Operation(summary = "gets applied price for a product at the specified quantity")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200",
          description = "returns the price for the product in the request",
          content = @Content(schema = @Schema(implementation = ProductPriceDto.class))),
      @ApiResponse(responseCode = "404",
          description = "Specified product does not have a price",
          content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "422",
          description = "Mandatory parameters are missing. (e.g. Product code)",
          content = @Content(schema = @Schema())),
  })
  @GetMapping
  ResponseEntity<ProductPriceDto> getPrice(
      @Parameter(description = "Code of the catalog. Optional, it acts as a filter")
      @RequestParam(value = "catalogCode", required = false) String catalogCode,

      @Parameter(description = "code of the product. Cannot be empty.", required = true)
      @RequestParam("productCode") String productCode,

      @Parameter(description = "Desired Quantity. Cannot be empty.", required = true)
      @RequestParam("quantity") BigDecimal quantity,

      @Parameter(description = "Desired Currency ISO code. Cannot be empty.", required = true)
      @RequestParam("currencyIsoCode") String currencyIsoCode);
}
