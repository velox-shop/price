package shop.velox.price.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import shop.velox.price.api.dto.CreatePriceDto;
import shop.velox.price.api.dto.PriceDto;
import shop.velox.price.api.dto.PriceStatus;

@Tag(name = "Price", description = "the Price API")
@RequestMapping("")
public interface PriceController {

  String PRICES_PATH = "/prices";
  String CURRENCY_ID_FILTER = "currencyId";
  String CURRENCY_ISOCODE_FILTER = "currencyIsoCode";
  String PRODUCT_CODE_FILTER = "productCode";
  String PRICE_STATUS_FILTER = "statusFilter";

  @Operation(summary = "Get Prices, sorted by startDate ascending", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200",
          description = "successful operation",
          content = @Content(schema = @Schema(implementation = PriceDto.class))),
      @ApiResponse(responseCode = "422",
          description = "Invalid filter combination",
          content = @Content(schema = @Schema())),
  })
  @GetMapping(value = PRICES_PATH)
  ResponseEntity<Page<PriceDto>> getPrices(
      @SortDefault.SortDefaults({@SortDefault(sort = "validFrom", direction = Sort.Direction.ASC)})
      Pageable pageable,
      @Parameter(description = """
          Filter by code of the Product.
          If list is not empty, return all prices matching any the given product codes""")
      @RequestParam(name = PRODUCT_CODE_FILTER, required = false) final List<String> productCodes,

      @Parameter(description = "Return prices with different status (all, active, upcoming, inactive) of the Price. If statusFilter is empty, return all prices. Can be empty.")
      @RequestParam(name = PRICE_STATUS_FILTER, required = false) final PriceStatus filter,

      @Parameter(description = "Return prices for the given currency ID. Cannot be mixed with "
          + CURRENCY_ISOCODE_FILTER)
      @RequestParam(name = CURRENCY_ID_FILTER, required = false) final String currencyId,

      @Parameter(description = "Return prices for the given currency IsoCode. Cannot be mixed with "
          + CURRENCY_ID_FILTER)
      @RequestParam(name = CURRENCY_ISOCODE_FILTER, required = false) final String currencyIsoCode
  );


  @Operation(summary = "Get Price by id", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation",
          content = @Content(schema = @Schema(implementation = PriceDto.class))),
      @ApiResponse(responseCode = "404", description = "price not found")
  })
  @GetMapping(value = PRICES_PATH + "/{id}")
  ResponseEntity<PriceDto> getPrice(
      @Parameter(description = "Id of the Price. Cannot be empty.", required = true) @PathVariable("id") final String id);


  @Operation(summary = "creates Price", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "returns the created price",
          content = @Content(schema = @Schema(implementation = PriceDto.class))),
      @ApiResponse(responseCode = "422", description = "Mandatory parameters are missing. (e.g. Product Code)",
          content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "403", description = "Unauthorized",
          content = @Content(schema = @Schema())),
  })
  @PostMapping(value = PRICES_PATH, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<PriceDto> createPrice(
      @Parameter(hidden = true)
      @AuthenticationPrincipal(expression = "T(shop.velox.commons.security.utils.AuthUtils).principalToUserIdMapper.apply(#this)")
      String userId,

      @Parameter(description = "Price to create. Cannot be empty.", required = true)
      @Valid @RequestBody CreatePriceDto prices);

  @Operation(summary = "Changes the acivation of the price", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation",
          content = @Content(schema = @Schema(implementation = PriceDto.class))),
      @ApiResponse(responseCode = "404", description = "price not found",
          content = @Content(schema = @Schema())),
  })
  @PatchMapping(value = PRICES_PATH + "/{id}")
  ResponseEntity<PriceDto> changePriceActivation(
      @Parameter(description = "Id of the Price. Cannot be empty.", required = true)
      @PathVariable("id") String id,

      @Parameter(description = "Changes the acivation of the price. Cannot be empty.", required = true)
      @RequestParam("active") Boolean active);


}
