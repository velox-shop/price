package shop.velox.price.api.controller.impl;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNoneBlank;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.price.api.controller.PriceController;
import shop.velox.price.api.dto.CreatePriceDto;
import shop.velox.price.api.dto.PriceDto;
import shop.velox.price.api.dto.PriceStatus;
import shop.velox.price.converter.impl.CreatePriceConverter;
import shop.velox.price.converter.impl.PriceConverter;
import shop.velox.price.model.PriceEntity;
import shop.velox.price.service.PriceService;

@RestController
@RequiredArgsConstructor
@Slf4j
public class PriceControllerImpl implements PriceController {

  private final PriceService priceService;

  private final PriceConverter priceConverter;

  private final CreatePriceConverter createPriceConverter;

  @Override

  public ResponseEntity<PriceDto> createPrice(final String userId, final CreatePriceDto price) {
    log.info("{} is creating Price: {}", userId, price);
    PriceDto body = priceConverter
        .convertEntityToDto(priceService.createPrice(createPriceConverter.convert(price)));
    log.info("{} has created Price: {}", userId, body);
    return ResponseEntity.status(HttpStatus.CREATED).body(body);
  }

  @Override
  public ResponseEntity<PriceDto> getPrice(final String id) {
    PriceEntity priceEntity = priceService.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    PriceDto priceDto = priceConverter.convertEntityToDto(priceEntity);
    return ResponseEntity.status(HttpStatus.OK).body(priceDto);
  }

  @Override
  public ResponseEntity<Page<PriceDto>> getPrices(Pageable pageable,
      final List<String> productCodes,
      final PriceStatus filter, final String currencyId, final String currencyIsoCode) {
    Page<PriceEntity> entitiesPage;
    if (isNoneBlank(currencyId, currencyIsoCode)) {
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
          String.format("%s and %s cannot be used together", CURRENCY_ID_FILTER,
              CURRENCY_ISOCODE_FILTER));
    }
    if (isEmpty(productCodes)) {
      entitiesPage = priceService.findAll(pageable, filter, currencyId, currencyIsoCode);
    } else {
      entitiesPage = priceService
          .findAllByProductCodes(pageable, productCodes, filter, currencyId, currencyIsoCode);
    }

    Page<PriceDto> dtosPage = priceConverter.convert(pageable, entitiesPage);
    return ResponseEntity.ok(dtosPage);
  }

  @Override
  public ResponseEntity<PriceDto> changePriceActivation(final String id, final Boolean active) {
    log.info("changePriceActivation {}, {}", id, active);
    PriceEntity priceEntity = priceService.update(id, active);
    return ResponseEntity.ok(priceConverter.convertEntityToDto(priceEntity));
  }

  @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
    return ex.getBindingResult()
        .getAllErrors()
        .stream()
        .collect(Collectors.toMap(
            error -> ((FieldError) error).getField(),
            ObjectError::getDefaultMessage));
  }
}
