package shop.velox.price.api.controller.impl;

import static org.springframework.http.HttpStatus.NOT_FOUND;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.price.api.controller.ProductPriceController;
import shop.velox.price.api.dto.ProductPriceDto;
import shop.velox.price.api.dto.ProductPriceListDto;
import shop.velox.price.api.dto.ProductPriceRequestDto;
import shop.velox.price.api.dto.ProductPriceRequestListDto;
import shop.velox.price.service.PriceService;

@RestController
@RequiredArgsConstructor
@Slf4j
public class ProductPriceControllerImpl implements ProductPriceController {

  private final PriceService priceService;

  @Override
  public ResponseEntity<ProductPriceListDto> getPrices(
      final ProductPriceRequestListDto productPriceRequestListDto) {
    List<ProductPriceDto> productsWithPrices = productPriceRequestListDto.getProductPriceRequests()
        .stream()
        .map(this::getPriceInternal)
        .filter(Optional::isPresent)
        .map(Optional::get)
        .toList();
    HttpStatus httpStatus =
        productPriceRequestListDto.getProductPriceRequests().size() == productsWithPrices.size()
            ? HttpStatus.OK
            : HttpStatus.PARTIAL_CONTENT;
    return ResponseEntity.status(httpStatus).body(new ProductPriceListDto(productsWithPrices));
  }

  @Override
  public ResponseEntity<ProductPriceDto> getPrice(String catalogCode, String productCode,
      BigDecimal quantity, String currencyIsoCode) {

    var requestDto = ProductPriceRequestDto.builder()
        .catalogCode(catalogCode)
        .productCode(productCode)
        .quantity(quantity)
        .currencyIsoCode(currencyIsoCode)
        .build();

    var responseDto = getPriceInternal(requestDto).orElseThrow(
        () -> new ResponseStatusException(NOT_FOUND));

    return ResponseEntity.status(HttpStatus.OK).body(responseDto);
  }

  private Optional<ProductPriceDto> getPriceInternal(ProductPriceRequestDto productPriceDto) {

    if (StringUtils.isEmpty(productPriceDto.getProductCode())) {
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Missing ProductCode");
    }
    if (StringUtils.isEmpty(productPriceDto.getQuantity())) {
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
          "ProductPrice " + productPriceDto.getProductCode() + " without quantity");
    }
    if (StringUtils.isEmpty(productPriceDto.getCurrencyIsoCode())) {
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
          "ProductPrice " + productPriceDto.getProductCode() + " without currency");
    }

    Optional<ProductPriceDto> result = priceService
        .getAppliedPrice(productPriceDto.getProductCode(), productPriceDto.getCatalogCode(),
            productPriceDto.getQuantity(), productPriceDto.getCurrencyIsoCode())
        .map(priceEntity -> priceService.applyPrice(priceEntity, productPriceDto.getQuantity()));

    log.debug("getPriceInternal for: {} returns {}", productPriceDto, result);

    return result;
  }
}
