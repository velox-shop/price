package shop.velox.price.api.dto;

import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import jakarta.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import shop.velox.price.model.PriceEntity;

@Data
@Builder
@Jacksonized
@FieldNameConstants
public class CreatePriceDto {

  @Size(min = 1, max = 50)
  @Builder.Default
  private String catalogCode = PriceEntity.CATALOG_CODE_DEFAULT;

  @Size(min = 3, max = 50)
  @NotNull
  private String productCode;

  @Builder.Default
  private LocalDateTime validFrom = PriceEntity.VALID_FROM_DEFAULT;

  @Builder.Default
  @Future
  private LocalDateTime validTo = PriceEntity.VALID_TO_DEFAULT;

  @PositiveOrZero
  @NotNull
  @Builder.Default
  private BigDecimal minQuantity = PriceEntity.MIN_QUANTITY_DEFAULT;

  @PositiveOrZero
  @NotNull
  private BigDecimal unitPrice;

  @Builder.Default
  private boolean active = true;

  @NotNull
  @Size(min = 3, max = 3)
  private String currencyIsoCode;
}
