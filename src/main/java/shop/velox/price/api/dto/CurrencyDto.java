package shop.velox.price.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

@Data
@Builder
@Jacksonized
public class CurrencyDto {

  @Schema(description = "Id of the currency.", example = "2e5c9ba8-956e-476b-816a-49ee128a40c9")
  @Size(min = 36, max = 36) // 36 is UUID size
  private String id;

  @Schema(description = "IsoCode of the currency", example = "EUR", required = true)
  @Size(min = 3, max = 3)
  @NotNull
  private String isoCode;

  @Schema(description = "Name of the currency", example = "Euro", required = true)
  @Size(min = 3, max = 50)
  private String name;

  @Schema(description = "Symbol of the Currency", example = "€", required = true)
  @Size(min = 1, max = 50)
  private String symbol;

}
