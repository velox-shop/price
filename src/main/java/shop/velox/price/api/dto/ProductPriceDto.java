package shop.velox.price.api.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import jakarta.validation.constraints.Size;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

@Data
@AllArgsConstructor
@Builder
@Jacksonized
public class ProductPriceDto {

  @Size(min = 3, max = 50)
  private String catalogCode;

  @Size(min = 3, max = 50)
  @NotNull
  private String productCode;

  @PositiveOrZero
  @NotNull
  private BigDecimal quantity;

  @PositiveOrZero
  @NotNull
  private BigDecimal totalPrice;

  @PositiveOrZero
  @NotNull
  private BigDecimal unitPrice;

  @NotNull
  @Size(min = 3, max = 3)
  private String currencyIsoCode;

}
