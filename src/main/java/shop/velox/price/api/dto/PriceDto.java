package shop.velox.price.api.dto;

import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import jakarta.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Data
@Builder
@Jacksonized
@FieldNameConstants
public class PriceDto {

  @Size(min = 36, max = 36) // 36 is UUID size
  private String id;

  @Size(min = 1, max = 50)
  private String catalogCode;

  @Size(min = 1, max = 50)
  @NotNull
  private String productCode;

  @NotNull
  private LocalDateTime validFrom;

  @NotNull
  @Future
  private LocalDateTime validTo;

  @PositiveOrZero
  @NotNull
  private BigDecimal minQuantity;

  @PositiveOrZero
  @NotNull
  private BigDecimal unitPrice;

  private boolean active;

  @NotNull
  private CurrencyDto currency;
}
