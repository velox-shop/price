package shop.velox.price.service;

import static org.assertj.core.api.Assertions.assertThat;
import static shop.velox.price.dao.CurrencyUtils.createIfMissing;

import jakarta.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import shop.velox.price.AbstractIntegrationTest;
import shop.velox.price.dao.CurrencyRepository;
import shop.velox.price.dao.CurrencyUtils;
import shop.velox.price.dao.PriceRepository;
import shop.velox.price.model.PriceEntity;

public class PriceServiceIntegrationTest extends AbstractIntegrationTest {

  @Resource
  private CurrencyRepository currencyRepository;

  @Resource
  private PriceRepository priceRepository;

  @Resource
  private PriceService priceService;

  @Test
  @Order(1)
  void findAllByFiltersTest() {
    // If
    var currencyEntity = createIfMissing(CurrencyUtils.CHF_CURRENCY_SUPPLIER.get(),
        currencyRepository);

    var productCode = "aProductCode";

    var price1Id = priceRepository.save(
            PriceEntity.builder()
                .productCode(productCode)
                .minQuantity(BigDecimal.valueOf(2L))
                .unitPrice(BigDecimal.valueOf(123.45))
                .currency(currencyEntity)
                .build())
        .getId();

    var price2Id = priceRepository.save(
            PriceEntity.builder()
                .productCode(productCode)
                .minQuantity(BigDecimal.valueOf(1L))
                .unitPrice(BigDecimal.valueOf(999.99))
                .currency(currencyEntity)
                .build())
        .getId();

    var priceForOtherProductCode = priceRepository.save(
            PriceEntity.builder()
                .productCode("otherProductCode")
                .unitPrice(BigDecimal.valueOf(123.45))
                .currency(currencyEntity)
                .build())
        .getId();

    var priceInactiveId = priceRepository.save(
            PriceEntity.builder()
                .productCode(productCode)
                .minQuantity(BigDecimal.valueOf(3L))
                .unitPrice(BigDecimal.valueOf(123.44))
                .currency(currencyEntity)
                .active(false)
                .build())
        .getId();

    var notStartedId = priceRepository.save(
            PriceEntity.builder()
                .productCode(productCode)
                .minQuantity(BigDecimal.valueOf(0L))
                .unitPrice(BigDecimal.valueOf(123.45))
                .currency(currencyEntity)
                .validFrom(LocalDateTime.now().plusDays(10))
                .validTo(LocalDateTime.now().plusDays(20))
                .build())
        .getId();

    var expiredId = priceRepository.save(
            PriceEntity.builder()
                .productCode(productCode)
                .minQuantity(BigDecimal.valueOf(0L))
                .unitPrice(BigDecimal.valueOf(123.45))
                .currency(currencyEntity)
                .validFrom(LocalDateTime.now().minusDays(20))
                .validTo(LocalDateTime.now().minusDays(10))
                .build())
        .getId();

    var minQuantityTooHighId = priceRepository.save(
            PriceEntity.builder()
                .productCode(productCode)
                .minQuantity(BigDecimal.valueOf(10L))
                .unitPrice(BigDecimal.valueOf(123.45))
                .currency(currencyEntity)
                .build())
        .getId();

    // When
    Optional<PriceEntity> resultOptional = priceService.getAppliedPrice(productCode, null, BigDecimal.valueOf(5),
        currencyEntity.getIsoCode());

    // Then
    assertThat(resultOptional).isNotEmpty();
    PriceEntity result = resultOptional.get();
    assertThat(result)
        .extracting(PriceEntity::getId)
        .isEqualTo(price1Id);
  }

}
