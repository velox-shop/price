package shop.velox.price.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.security.test.context.support.WithMockUser;
import shop.velox.price.AbstractIntegrationTest;
import shop.velox.price.dao.CurrencyRepository;
import shop.velox.price.dao.CurrencyUtils;
import shop.velox.price.model.CurrencyEntity;

public class CurrencyServiceIntegrationTest extends AbstractIntegrationTest {

  @Resource
  private CurrencyService currencyService;

  @Resource
  private CurrencyRepository currencyRepository;

  @Test
  @WithMockUser(authorities = {"Admin"})
  void updateCurrencyTest() {
    var currency = CurrencyUtils.createIfMissing(CurrencyUtils.CHF_CURRENCY_SUPPLIER.get(),
        currencyRepository);

    var updatedCurrency = currencyService.update(currency.getId(), CurrencyEntity.builder()
        .isoCode("CHF")
        .symbol("newSymbol")
        .name("newName")
        .build());

    assertEquals(updatedCurrency.getId(), currency.getId());
  }

}
