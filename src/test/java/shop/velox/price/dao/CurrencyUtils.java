package shop.velox.price.dao;

import java.util.function.Supplier;
import lombok.experimental.UtilityClass;
import shop.velox.price.model.CurrencyEntity;

@UtilityClass
public class CurrencyUtils {

  public static final Supplier<CurrencyEntity> CHF_CURRENCY_SUPPLIER = () -> CurrencyEntity.builder()
      .isoCode("CHF")
      .name("Swiss franc")
      .symbol("CHF")
      .build();

  public static final Supplier<CurrencyEntity> EUR_CURRENCY_SUPPLIER = () -> CurrencyEntity.builder()
      .isoCode("EUR")
      .name("Euro")
      .symbol("€")
      .build();

  public static CurrencyEntity createIfMissing(CurrencyEntity currencyEntity,
      CurrencyRepository currencyRepository) {
    return currencyRepository.findOneByIsoCode(currencyEntity.getIsoCode())
        .orElseGet(() -> currencyRepository.save(currencyEntity));
  }

}
