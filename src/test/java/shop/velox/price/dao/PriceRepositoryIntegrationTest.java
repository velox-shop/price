package shop.velox.price.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static shop.velox.price.dao.CurrencyUtils.CHF_CURRENCY_SUPPLIER;

import jakarta.annotation.Resource;
import java.math.BigDecimal;
import java.util.function.Supplier;
import org.apache.commons.lang3.RandomStringUtils;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import shop.velox.price.model.CurrencyEntity;
import shop.velox.price.model.PriceEntity;

class PriceRepositoryIntegrationTest extends AbstractJpaTest {

  @Resource
  private PriceRepository priceRepository;

  @Resource
  private CurrencyRepository currencyRepository;

  @Test
  @Order(1)
  public void auditTest() {
    CurrencyEntity currencyEntity = getCurrencyEntity();

    PriceEntity savedEntity = priceRepository.save(
        PriceEntity.builder()
            .productCode("aProductCode")
            .unitPrice(BigDecimal.valueOf(123.45))
            .currency(currencyEntity)
            .build());

    assertNotNull(savedEntity.getCreateTime());
    assertNotNull(savedEntity.getModifiedTime());
  }

  @Test
  @Order(2)
  public void compositeConstraintUniqueWithoutCatalogCodeTest() {
    CurrencyEntity currencyEntity = getCurrencyEntity();
    String productCode = RandomStringUtils.randomAlphabetic(10);

    Supplier<PriceEntity> priceSupplier = () -> priceRepository.save(
        PriceEntity.builder()
            .productCode(productCode)
            .unitPrice(BigDecimal.valueOf(123.45))
            .currency(currencyEntity)
            .build());

    Assertions.assertDoesNotThrow(priceSupplier::get);
    assertEquals(1L, priceRepository.findAll(Pageable.unpaged()).getTotalElements());

    Assertions.assertThrows(DataIntegrityViolationException.class, priceSupplier::get);
  }

  @Test
  @Order(2)
  public void compositeConstraintUniqueWithCatalogCodeTest() {
    CurrencyEntity currencyEntity = getCurrencyEntity();
    String productCode = RandomStringUtils.randomAlphabetic(10);

    Supplier<PriceEntity> priceSupplier = () -> priceRepository.save(
        PriceEntity.builder()
            .catalogCode("aCatalogCode")
            .productCode(productCode)
            .unitPrice(BigDecimal.valueOf(123.45))
            .currency(currencyEntity)
            .build());

    Assertions.assertDoesNotThrow(priceSupplier::get);
    assertEquals(1L, priceRepository.findAll(Pageable.unpaged()).getTotalElements());

    Assertions.assertThrows(DataIntegrityViolationException.class, priceSupplier::get);
  }

  @Test
  @Order(4)
  void findByIdTest() {
    CurrencyEntity currencyEntity = getCurrencyEntity();

    PriceEntity existing = priceRepository.save(
        PriceEntity.builder()
            .productCode("aProductCode")
            .unitPrice(BigDecimal.valueOf(123.45))
            .currency(currencyEntity)
            .build());

    assertThat(priceRepository.findOneByIdAndActiveIsTrue(existing.getId())).isNotEmpty();

    existing.setActive(false);
    priceRepository.save(existing);

    assertThat(priceRepository.findOneByIdAndActiveIsTrue(existing.getId())).isEmpty();
  }

  private CurrencyEntity getCurrencyEntity() {
    return currencyRepository.save(CHF_CURRENCY_SUPPLIER.get());
  }

  @Test
  void createPrice() {
    PriceEntity priceEntity = priceRepository.save(PriceEntity.builder()
        .productCode(RandomStringUtils.randomAlphabetic(10))
        .currency(getCurrencyEntity())
        .unitPrice(new BigDecimal("123.45"))
        .build());

    // check that Defaults work
    MatcherAssert.assertThat(priceEntity.getValidFrom(), is(notNullValue()));
  }

  @Test
  void createPriceIn2DifferentCatalogs() {

    CurrencyEntity currencyEntity = getCurrencyEntity();
    String productCode = RandomStringUtils.randomAlphabetic(10);
    BigDecimal unitPrice = new BigDecimal("123.45");

    priceRepository.save(
        PriceEntity.builder()
            .catalogCode("catalogCode1")
            .productCode(productCode)
            .currency(currencyEntity)
            .unitPrice(unitPrice)
            .build());

    PriceEntity entityToSave = PriceEntity.builder()
        .catalogCode("catalogCode2")
        .productCode(productCode)
        .currency(currencyEntity)
        .unitPrice(unitPrice)
        .build();

    assertDoesNotThrow(() -> priceRepository.save(entityToSave));
  }

  @Test
  void createPriceWithAndWithoutCatalogs() {
    CurrencyEntity currencyEntity = getCurrencyEntity();
    String productCode = RandomStringUtils.randomAlphabetic(10);
    BigDecimal unitPrice = new BigDecimal("123.45");

    priceRepository.save(
        PriceEntity.builder()
            .catalogCode("catalogCode1")
            .productCode(productCode)
            .currency(currencyEntity)
            .unitPrice(unitPrice)
            .build());

    PriceEntity entityToSave = PriceEntity.builder()
        .catalogCode(null)
        .productCode(productCode)
        .currency(currencyEntity)
        .unitPrice(unitPrice)
        .build();

    assertDoesNotThrow(() -> priceRepository.save(entityToSave));
  }

}
