package shop.velox.price.dao;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static shop.velox.price.dao.CurrencyUtils.CHF_CURRENCY_SUPPLIER;

import jakarta.annotation.Resource;
import jakarta.transaction.Transactional;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import shop.velox.price.model.CurrencyEntity;
import shop.velox.price.model.CurrencyEntity.Fields;

@ExtendWith(SpringExtension.class)
@Transactional
class CurrencyRepositoryIntegrationTest extends AbstractJpaTest {

  @Resource
  private CurrencyRepository currencyRepository;

  @Test
  void findOneById() {
    // Given
    CurrencyEntity currency = currencyRepository.save(CHF_CURRENCY_SUPPLIER.get());

    // When
    Optional<CurrencyEntity> result = currencyRepository.findOneById(currency.getId());

    // Then
    assertTrue(result.isPresent());
    assertThat(result.get(), hasProperty(Fields.id, equalTo(currency.getId())));
    assertThat(result.get(), hasProperty(Fields.isoCode, equalTo(currency.getIsoCode())));
  }

  @Test
  void findOneByIsoCode() {
    // Given
    CurrencyEntity currency = currencyRepository.save(CHF_CURRENCY_SUPPLIER.get());

    // When
    Optional<CurrencyEntity> result = currencyRepository.findOneByIsoCode(currency.getIsoCode());

    // Then
    assertTrue(result.isPresent());
    assertThat(result.get(), hasProperty(Fields.id, equalTo(currency.getId())));
    assertThat(result.get(), hasProperty(Fields.isoCode, equalTo(currency.getIsoCode())));
  }

  @Test
  void avoidIsoCodeDuplicates() {

    assertDoesNotThrow(() -> currencyRepository.save(CHF_CURRENCY_SUPPLIER.get()));

    assertThrows(DataIntegrityViolationException.class,
        () -> currencyRepository.save(CHF_CURRENCY_SUPPLIER.get()));

  }

}
