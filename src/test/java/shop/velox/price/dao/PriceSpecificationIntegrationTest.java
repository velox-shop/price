package shop.velox.price.dao;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;

import jakarta.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import shop.velox.price.api.dto.PriceStatus;
import shop.velox.price.model.CurrencyEntity;
import shop.velox.price.model.PriceEntity;
import shop.velox.price.utils.PriceEntityByIdMatcher;

@Slf4j
class PriceSpecificationIntegrationTest extends AbstractJpaTest {

  @Resource
  private PriceRepository priceRepository;

  @Resource
  private CurrencyRepository currencyRepository;

  void cleanDb() {
    var prices = priceRepository.count();
    if (prices > 0) {
      log.info("Deleting prices: {}", prices);
      priceRepository.deleteAll();
    }

    var currencies = currencyRepository.count();
    if (currencies > 0) {
      log.info("Deleting currencies: {}", currencies);
      currencyRepository.deleteAll();
    }
  }

  @Test
  void isProductCodeIn() {
    // Given
    CurrencyEntity currency = currencyRepository
        .save(CurrencyUtils.CHF_CURRENCY_SUPPLIER.get());

    PriceEntity price1 = priceRepository.save(
        PriceEntity.builder()
            .productCode("001")
            .unitPrice(new BigDecimal(10))
            .currency(currency)
            .build());
    PriceEntity price2 = priceRepository.save(PriceEntity.builder()
        .productCode("002")
        .unitPrice(new BigDecimal(12))
        .currency(currency)
        .build());

    assertThat(priceRepository.findAll(), contains(price1, price2));

    // When
    List<PriceEntity> result = priceRepository
        .findAll(PriceSpecification.isProductCodeIn(List.of(price1.getProductCode())));

    // Then
    assertThat(result, hasSize(1));
    assertThat(result, contains(price1));
  }

  @Test
  void isCurrencyEqualUsingId() {
    // Given
    cleanDb();

    CurrencyEntity currency1 = currencyRepository.save(
        CurrencyUtils.CHF_CURRENCY_SUPPLIER.get());
    CurrencyEntity currency2 = currencyRepository.save(
        CurrencyUtils.EUR_CURRENCY_SUPPLIER.get());

    PriceEntity price1 = priceRepository.save(
        PriceEntity.builder()
            .productCode("001")
            .unitPrice(new BigDecimal(10))
            .currency(currency1)
            .build());
    PriceEntity price2 = priceRepository.save(
        PriceEntity.builder()
            .productCode("002")
            .unitPrice(new BigDecimal(12))
            .currency(currency2)
            .build());

    assertThat(priceRepository.findAll(), contains(price1, price2));

    // When
    List<PriceEntity> result = priceRepository
        .findAll(PriceSpecification.isCurrencyEqual(currency2.getId(), null));

    // Then
    assertThat(result, contains(price2));
  }

  @Test
  void isCurrencyEqualUsingIsoCode() {
    // Given
    CurrencyEntity currency1 = currencyRepository.save(
        CurrencyUtils.CHF_CURRENCY_SUPPLIER.get());
    CurrencyEntity currency2 = currencyRepository.save(
        CurrencyUtils.EUR_CURRENCY_SUPPLIER.get());

    PriceEntity price1 = priceRepository.save(
        PriceEntity.builder()
            .productCode("001")
            .unitPrice(new BigDecimal(10))
            .currency(currency1)
            .build());
    PriceEntity price2 = priceRepository.save(PriceEntity.builder()
        .productCode("002")
        .unitPrice(new BigDecimal(12))
        .currency(currency2)
        .build());

    List<PriceEntity> allPrices = priceRepository.findAll();
    assertThat(allPrices, containsInAnyOrder(price1, price2));

    // When
    List<PriceEntity> result = priceRepository
        .findAll(PriceSpecification.isCurrencyEqual(null, currency2.getIsoCode()));

    // Then
    assertThat(result, contains(price2));
  }


  @Test
  void testEmptySpecification() {
    // Given
    String productCode = "aProductCode";
    CurrencyEntity currencyEntity = currencyRepository.save(
        CurrencyUtils.CHF_CURRENCY_SUPPLIER.get());

    var priceActive = priceRepository.save(
        PriceEntity.builder()
            .productCode(productCode)
            .minQuantity(BigDecimal.valueOf(2L))
            .unitPrice(BigDecimal.valueOf(123.44))
            .currency(currencyEntity)
            .build());

    // When
    List<PriceEntity> result = priceRepository.findAll(
        PriceSpecification.isProductCodeIn(null)
            .and(PriceSpecification.hasStatus(null)
                .and(PriceSpecification.isCurrencyEqual(null, null))));

    // Then
    assertThat(result, contains(priceActive));
  }

  @Nested
  @TestInstance(TestInstance.Lifecycle.PER_CLASS)
  class HasStatusTest extends AbstractJpaTest {

    @ParameterizedTest(autoCloseArguments = false)
    @MethodSource("hasStatusTestArguments")
    void hasStatusTest(PriceStatus priceStatus, List<PriceEntity> expectedResults) {

      // When
      List<PriceEntity> result = priceRepository
          .findAll(PriceSpecification.hasStatus(priceStatus));

      // Then
      assertThat(result, new PriceEntityByIdMatcher(expectedResults));
    }

    private Stream<Arguments> hasStatusTestArguments() {
      // Given
      log.info("hasStatusTestArguments START");
      cleanDb();
      String productCode = "aProductCode";
      CurrencyEntity currencyEntity = currencyRepository.save(
          CurrencyUtils.CHF_CURRENCY_SUPPLIER.get());

      var priceActive = priceRepository.save(
          PriceEntity.builder()
              .productCode(productCode)
              .minQuantity(BigDecimal.valueOf(2.00))
              .unitPrice(BigDecimal.valueOf(123.45))
              .currency(currencyEntity)
              .build());

      var priceInactive = priceRepository.save(
          PriceEntity.builder()
              .productCode(productCode)
              .minQuantity(BigDecimal.valueOf(3.00))
              .unitPrice(BigDecimal.valueOf(123.44))
              .currency(currencyEntity)
              .active(false)
              .build());

      var notStarted = priceRepository.save(
          PriceEntity.builder()
              .productCode(productCode)
              .minQuantity(BigDecimal.valueOf(2.00))
              .unitPrice(BigDecimal.valueOf(123.45))
              .currency(currencyEntity)
              .validFrom(LocalDateTime.now().plusDays(10))
              .validTo(LocalDateTime.now().plusDays(20))
              .build());

      var expired = priceRepository.save(
          PriceEntity.builder()
              .productCode(productCode)
              .minQuantity(BigDecimal.valueOf(2.00))
              .unitPrice(BigDecimal.valueOf(123.45))
              .currency(currencyEntity)
              .validFrom(LocalDateTime.now().minusDays(20))
              .validTo(LocalDateTime.now().minusDays(10))
              .build());

      log.info("hasStatusTestArguments END");

      return Stream.of(
          Arguments.of(PriceStatus.ACTIVE, List.of(priceActive)),
          Arguments.of(PriceStatus.INACTIVE, List.of(priceInactive, expired)),
          Arguments.of(PriceStatus.UPCOMING, List.of(notStarted))
      );
    }
  }

  @Test
  void isActiveTest() {
    CurrencyEntity currency1 = currencyRepository.save(
        CurrencyUtils.CHF_CURRENCY_SUPPLIER.get());
    CurrencyEntity currency2 = currencyRepository.save(
        CurrencyUtils.EUR_CURRENCY_SUPPLIER.get());

    var active = priceRepository.save(PriceEntity.builder()
        .productCode(RandomStringUtils.randomAlphabetic(10))
        .minQuantity(BigDecimal.ONE)
        .unitPrice(BigDecimal.TEN)
        .currency(currency1)
        .active(true)
        .build());

    var inactive = priceRepository.save(PriceEntity.builder()
        .productCode(RandomStringUtils.randomAlphabetic(10))
        .minQuantity(BigDecimal.ONE)
        .unitPrice(BigDecimal.TEN)
        .currency(currency2)
        .active(false)
        .build());

    var results = priceRepository.findAll(PriceSpecification.isActive());

    assertThat(results, contains(active));
  }

  @Test
  void isCurrentTest() {
    CurrencyEntity currency1 = currencyRepository.save(
        CurrencyUtils.CHF_CURRENCY_SUPPLIER.get());
    CurrencyEntity currency2 = currencyRepository.save(
        CurrencyUtils.EUR_CURRENCY_SUPPLIER.get());

    var current = priceRepository.save(PriceEntity.builder()
        .productCode(RandomStringUtils.randomAlphabetic(10))
        .minQuantity(BigDecimal.ONE)
        .unitPrice(BigDecimal.TEN)
        .currency(currency1)
        .active(true)
        .validFrom(LocalDateTime.now().minusDays(1))
        .validTo(LocalDateTime.now().plusDays(1))
        .build());

    var past = priceRepository.save(PriceEntity.builder()
        .productCode(RandomStringUtils.randomAlphabetic(10))
        .minQuantity(BigDecimal.ONE)
        .unitPrice(BigDecimal.TEN)
        .currency(currency2)
        .active(true)
        .validFrom(LocalDateTime.now().minusDays(10))
        .validTo(LocalDateTime.now().minusDays(1))
        .build());

    var future = priceRepository.save(PriceEntity.builder()
        .productCode(RandomStringUtils.randomAlphabetic(10))
        .minQuantity(BigDecimal.ONE)
        .unitPrice(BigDecimal.TEN)
        .currency(currency1)
        .active(true)
        .validFrom(LocalDateTime.now().plusDays(1))
        .validTo(LocalDateTime.now().plusDays(10))
        .build());

    var results = priceRepository.findAll(PriceSpecification.isCurrent());

    assertThat(results, contains(current));
  }

  @Test
  void isMinQuantityLessThanOrEqualToTest() {
    CurrencyEntity currency1 = currencyRepository.save(
        CurrencyUtils.CHF_CURRENCY_SUPPLIER.get());

    var minQuantityOk = priceRepository.save(PriceEntity.builder()
        .productCode(RandomStringUtils.randomAlphabetic(10))
        .minQuantity(BigDecimal.ONE)
        .unitPrice(BigDecimal.TEN)
        .currency(currency1)
        .active(true)
        .build());

    var minQuantityTooHigh = priceRepository.save(PriceEntity.builder()
        .productCode(RandomStringUtils.randomAlphabetic(10))
        .minQuantity(BigDecimal.TEN)
        .unitPrice(BigDecimal.TEN)
        .currency(currency1)
        .active(true)
        .build());

    var results = priceRepository.findAll(
        PriceSpecification.isMinQuantityLessThanOrEqualTo(new BigDecimal("5")));

    assertThat(results, contains(minQuantityOk));
  }

  @Test
  void isCatalogCodeNullOrEqualToTest() {
    CurrencyEntity currency1 = currencyRepository.save(
        CurrencyUtils.CHF_CURRENCY_SUPPLIER.get());

    String catalogCode = "theCatalogCode";

    var withOKCatalogCode = priceRepository.save(PriceEntity.builder()
        .catalogCode(catalogCode)
        .productCode(RandomStringUtils.randomAlphabetic(10))
        .minQuantity(BigDecimal.ONE)
        .unitPrice(BigDecimal.TEN)
        .currency(currency1)
        .active(true)
        .build());

    var withoutCatalogCode = priceRepository.save(PriceEntity.builder()
        .productCode(RandomStringUtils.randomAlphabetic(10))
        .minQuantity(BigDecimal.ONE)
        .unitPrice(BigDecimal.TEN)
        .currency(currency1)
        .active(true)
        .build());

    var withOtherCatalogCode = priceRepository.save(PriceEntity.builder()
        .catalogCode("otherCatalogCode")
        .productCode(RandomStringUtils.randomAlphabetic(10))
        .minQuantity(BigDecimal.ONE)
        .unitPrice(BigDecimal.TEN)
        .currency(currency1)
        .active(true)
        .build());

    var results = priceRepository.findAll(
        PriceSpecification.isCatalogCodeMissingOrEqualTo(catalogCode));

    assertThat(results, containsInAnyOrder(withOKCatalogCode, withoutCatalogCode));
  }
}