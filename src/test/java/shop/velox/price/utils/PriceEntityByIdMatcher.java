package shop.velox.price.utils;

import java.util.List;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import shop.velox.price.model.PriceEntity;

public class PriceEntityByIdMatcher extends TypeSafeMatcher<List<PriceEntity>> {

  private final List<PriceEntity> expectedList;

  public PriceEntityByIdMatcher(List<PriceEntity> expectedList) {
    this.expectedList = expectedList;
  }

  @Override
  protected boolean matchesSafely(List<PriceEntity> actualList) {
    if (expectedList.size() != actualList.size()) {
      return false;
    }

    for (int i = 0; i < expectedList.size(); i++) {
      if (!expectedList.get(i).getId().equals(actualList.get(i).getId())) {
        return false;
      }
    }

    return true;
  }

  @Override
  public void describeTo(Description description) {
    description.appendText("lists have the same PriceEntity objects based on the field 'id'");
  }
}

