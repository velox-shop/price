package shop.velox.price.controller;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import shop.velox.price.AbstractIntegrationTest;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public abstract class AbstractWebIntegrationTest extends AbstractIntegrationTest {

  @Value(value = "${local.server.port}")
  @Getter(AccessLevel.PROTECTED)
  protected int port;

  @Autowired
  @Getter(AccessLevel.PROTECTED)
  private TestRestTemplate anonymousRestTemplate;

  @Getter(AccessLevel.PROTECTED)
  private TestRestTemplate adminRestTemplate;

  @BeforeEach
  public void setup() throws InterruptedException {
    log.info("Web test method setup");

    // Support PATCH https://stackoverflow.com/a/29803488/1338898
    anonymousRestTemplate.getRestTemplate()
        .setRequestFactory(new HttpComponentsClientHttpRequestFactory());

    adminRestTemplate = anonymousRestTemplate.withBasicAuth("admin", "velox");
  }

}
