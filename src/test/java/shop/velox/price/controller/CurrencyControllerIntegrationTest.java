package shop.velox.price.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static shop.velox.price.controller.utils.CurrencyControllerUtils.updateCurrency;

import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import shop.velox.price.api.dto.CurrencyDto;
import shop.velox.price.dao.CurrencyRepository;
import shop.velox.price.dao.CurrencyUtils;

@Slf4j
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
public class CurrencyControllerIntegrationTest extends AbstractWebIntegrationTest {

  @Resource
  private CurrencyRepository currencyRepository;

  @Test
  @WithMockUser(authorities = {"Admin"})
  void updateCurrencyTest() {
    var currency = CurrencyUtils.createIfMissing(CurrencyUtils.CHF_CURRENCY_SUPPLIER.get(),
        currencyRepository);

    CurrencyDto payload = CurrencyDto.builder()
        .id(currency.getId())
        .isoCode("CHF")
        .symbol("newSymbol")
        .name("newName")
        .build();

    var currencyDtoResponseEntity = updateCurrency(currency.getId(), payload, getAdminRestTemplate(), getPort());

    assertEquals(HttpStatus.OK, currencyDtoResponseEntity.getStatusCode());
    assertNotNull(currencyDtoResponseEntity.getBody());
    assertEquals(currencyDtoResponseEntity.getBody().getId(), currency.getId());
  }

}
