package shop.velox.price.controller.utils;

import static org.springframework.http.HttpMethod.PATCH;

import java.net.URI;
import java.util.Map;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;
import shop.velox.price.api.dto.CurrencyDto;

@UtilityClass
@Slf4j
public class CurrencyControllerUtils {

  public static final String BASE_URL = "http://localhost:{port}/price/v1/currencies";
  public static ResponseEntity<CurrencyDto> updateCurrency(String currencyId, CurrencyDto payload,
      TestRestTemplate restTemplate, int port) {

    // Create HttpEntity with the payload
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity<CurrencyDto> httpEntity = new HttpEntity<>(payload, headers);

    var uriPathParams = Map.of("port", port);

    URI uri = UriComponentsBuilder.fromUriString(BASE_URL)
        .pathSegment(currencyId)
        .buildAndExpand(uriPathParams)
        .toUri();

    log.info("updateCurrency with URI: {}", uri);

    ParameterizedTypeReference<CurrencyDto> responseType = new ParameterizedTypeReference<>() {
    };

    return restTemplate.exchange(uri, PATCH, httpEntity, responseType);
  }

}
