package shop.velox.price.controller.utils;

import static org.springframework.http.HttpMethod.POST;

import java.net.URI;
import java.util.Map;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;
import shop.velox.price.api.dto.CreatePriceDto;
import shop.velox.price.api.dto.PriceDto;

@UtilityClass
@Slf4j
public class PriceControllerUtils {

  public static final String BASE_URL = "http://localhost:{port}/price/v1/prices";

  public static ResponseEntity<PriceDto> createPrice(CreatePriceDto payload,
      TestRestTemplate restTemplate, int port) {

    // Create HttpEntity with the payload
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity<CreatePriceDto> httpEntity = new HttpEntity<>(payload, headers);

    var uriPathParams = Map.of("port", port);

    URI uri = UriComponentsBuilder.fromUriString(BASE_URL)
        .buildAndExpand(uriPathParams)
        .toUri();

    log.info("createPrice with URI: {}", uri);

    ParameterizedTypeReference<PriceDto> responseType = new ParameterizedTypeReference<>() {
    };

    return restTemplate.exchange(uri, POST, httpEntity, responseType);
  }

}
