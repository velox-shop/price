package shop.velox.price.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static shop.velox.price.controller.utils.PriceControllerUtils.createPrice;

import jakarta.annotation.Resource;
import java.math.BigDecimal;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import shop.velox.price.api.dto.CreatePriceDto;
import shop.velox.price.api.dto.PriceDto;
import shop.velox.price.dao.CurrencyRepository;
import shop.velox.price.dao.CurrencyUtils;

@Slf4j
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
public class PriceControllerIntegrationTest extends AbstractWebIntegrationTest {

  @Resource
  private CurrencyRepository currencyRepository;

  @BeforeAll
  void setUp() {
    currencyRepository.save(CurrencyUtils.CHF_CURRENCY_SUPPLIER.get());
    currencyRepository.save(CurrencyUtils.EUR_CURRENCY_SUPPLIER.get());
  }

  @Test
  void createPriceWithoutCatalogCode() {
    CreatePriceDto payload = CreatePriceDto.builder()
        .productCode(RandomStringUtils.randomAlphabetic(10))
        .minQuantity(BigDecimal.ONE)
        .unitPrice(new BigDecimal("32.10"))
        .currencyIsoCode("CHF")
        .build();

    ResponseEntity<PriceDto> responseEntity = createPrice(payload, getAdminRestTemplate(),
        getPort());

    assertThat(responseEntity.getStatusCode(), equalTo(CREATED));
    PriceDto entityBody = responseEntity.getBody();
    assertThat(entityBody, is(notNullValue()));
    assertThat(entityBody.getCurrency(), is(notNullValue()));
    assertThat(entityBody.getCurrency().getIsoCode(), equalTo(payload.getCurrencyIsoCode()));

    assertThat(entityBody.getCatalogCode(), is(nullValue()));
  }

  @Test
  void createPriceWithCatalogCode() {
    CreatePriceDto payload = CreatePriceDto.builder()
        .catalogCode("aCatalogCode")
        .productCode(RandomStringUtils.randomAlphabetic(10))
        .minQuantity(BigDecimal.ONE)
        .unitPrice(new BigDecimal("32.10"))
        .currencyIsoCode("CHF")
        .build();

    ResponseEntity<PriceDto> responseEntity = createPrice(payload, getAdminRestTemplate(),
        getPort());

    assertThat(responseEntity.getStatusCode(), equalTo(CREATED));
    PriceDto entityBody = responseEntity.getBody();
    assertThat(entityBody, is(notNullValue()));
    assertThat(entityBody.getCurrency(), is(notNullValue()));
    assertThat(entityBody.getCurrency().getIsoCode(), equalTo(payload.getCurrencyIsoCode()));

    assertThat(entityBody.getCatalogCode(), equalTo(payload.getCatalogCode()));
  }

  @Test
  void createPriceAnonymous() {
    CreatePriceDto payload = CreatePriceDto.builder()
        .catalogCode("aCatalogCode")
        .productCode(RandomStringUtils.randomAlphabetic(10))
        .minQuantity(BigDecimal.ONE)
        .unitPrice(new BigDecimal("32.10"))
        .currencyIsoCode("CHF")
        .build();

    ResponseEntity<PriceDto> responseEntity = createPrice(payload, getAnonymousRestTemplate(),
        getPort());

    assertThat(responseEntity.getStatusCode(), equalTo(FORBIDDEN));
  }


}
