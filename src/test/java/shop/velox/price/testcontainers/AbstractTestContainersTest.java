package shop.velox.price.testcontainers;

import java.io.File;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Timeout;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.ContainerState;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import shop.velox.price.controller.AbstractWebIntegrationTest;

@Testcontainers
@Timeout(value = 2, unit = TimeUnit.MINUTES)
@ActiveProfiles(profiles = {"localauth", "testcontainers"}, inheritProfiles = false)
@Slf4j
public abstract class AbstractTestContainersTest extends AbstractWebIntegrationTest {

  public static final String MY_SQL_SERVICE_NAME = "mysqlservices_1";

  public static Supplier<DockerComposeContainer> environmentSupplier = () ->
      new DockerComposeContainer(new File("src/test/resources/docker-compose.yml"))
          .withExposedService(MY_SQL_SERVICE_NAME, 3306);

  @AfterEach
  void printLogs() {
    ContainerState mySqlContainer = (ContainerState) getEnvironment().getContainerByServiceName(
        MY_SQL_SERVICE_NAME).get();
    log.info("mySqlContainer Logs: {}", mySqlContainer.getLogs());
  }

  abstract DockerComposeContainer getEnvironment();

}
