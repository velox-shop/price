package shop.velox.price.testcontainers;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.springframework.http.HttpStatus.CREATED;
import static shop.velox.price.controller.utils.PriceControllerUtils.createPrice;
import static shop.velox.price.dao.CurrencyUtils.createIfMissing;

import jakarta.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.junit.jupiter.Container;
import shop.velox.price.api.dto.CreatePriceDto;
import shop.velox.price.api.dto.PriceDto;
import shop.velox.price.dao.CurrencyRepository;
import shop.velox.price.dao.CurrencyUtils;
import shop.velox.price.dao.PriceRepository;


public class PriceControllerDataIntegrityTest extends AbstractTestContainersTest {

  @Resource
  @Getter
  private PriceRepository priceRepository;

  @Resource
  private CurrencyRepository currencyRepository;

  @Container
  public static DockerComposeContainer environment = environmentSupplier.get();

  @BeforeEach
  void setUp() {
    createIfMissing(CurrencyUtils.CHF_CURRENCY_SUPPLIER.get(), currencyRepository);
    createIfMissing(CurrencyUtils.EUR_CURRENCY_SUPPLIER.get(), currencyRepository);
  }

  @Test
  @SneakyThrows
  void createPriceTwice() {
    // Given
    String productCode = RandomStringUtils.randomAlphabetic(10);
    Supplier<ResponseEntity<PriceDto>> responseEntityProvider = () -> createPrice(
        CreatePriceDto.builder()
            .productCode(productCode)
            .minQuantity(BigDecimal.ONE)
            .unitPrice(new BigDecimal("32.10"))
            .currencyIsoCode("CHF")
            .build(),
        getAdminRestTemplate(), getPort());

    ResponseEntity<PriceDto> firstSaveResponseEntity = responseEntityProvider.get();
    assertThat(firstSaveResponseEntity.getStatusCode(), equalTo(CREATED));
    PriceDto firstSavePrice = firstSaveResponseEntity.getBody();

    // When
    ResponseEntity<PriceDto> secondSaveResponseEntity = responseEntityProvider.get();

    // Then
    assertThat(secondSaveResponseEntity.getStatusCode(), equalTo(CREATED));
    PriceDto secondSavePrice = secondSaveResponseEntity.getBody();

    assertEquals(firstSavePrice.getId(), secondSavePrice.getId());

    assertActive(firstSavePrice);
    assertActive(secondSavePrice);
  }

  @Test
  @SneakyThrows
  void createPriceTwiceButDifferentUniqueProperty() {
    // Given
    String productCode = RandomStringUtils.randomAlphabetic(10);
    Supplier<ResponseEntity<PriceDto>> responseEntityProvider = () -> createPrice(
        CreatePriceDto.builder()
            .productCode(productCode)
            .minQuantity(BigDecimal.ONE)
            .unitPrice(new BigDecimal("32.10"))
            .validFrom(LocalDateTime.now().minusDays(1))
            .validTo(LocalDateTime.now().plusDays(1))
            .currencyIsoCode("CHF")
            .build(),
        getAdminRestTemplate(), getPort());

    ResponseEntity<PriceDto> firstSaveResponseEntity = responseEntityProvider.get();
    assertThat(firstSaveResponseEntity.getStatusCode(), equalTo(CREATED));
    PriceDto firstSavePrice = firstSaveResponseEntity.getBody();

    // When
    TimeUnit.SECONDS.sleep(1);
    ResponseEntity<PriceDto> secondSaveResponseEntity = responseEntityProvider.get();

    // Then
    assertThat(secondSaveResponseEntity.getStatusCode(), equalTo(CREATED));
    PriceDto secondSavePrice = secondSaveResponseEntity.getBody();

    assertNotEquals(firstSavePrice.getId(), secondSavePrice.getId());

    assertActive(firstSavePrice);
    assertActive(secondSavePrice);
  }

  @Test
  void createPriceTwiceButDifferentActivation() {

    String productCode = RandomStringUtils.randomAlphabetic(10);
    Function<Boolean, ResponseEntity<PriceDto>> responseEntityProvider = (Boolean active) -> createPrice(
        CreatePriceDto.builder()
            .productCode(productCode)
            .minQuantity(BigDecimal.ONE)
            .unitPrice(new BigDecimal("123.45"))
            .currencyIsoCode("CHF")
            .active(active)
            .build(),
        getAdminRestTemplate(), getPort());

    ResponseEntity<PriceDto> firstSaveResponseEntity = responseEntityProvider.apply(Boolean.TRUE);
    assertThat(firstSaveResponseEntity.getStatusCode(), equalTo(CREATED));
    PriceDto firstSavePrice = firstSaveResponseEntity.getBody();
    assertEquals(Boolean.TRUE, firstSavePrice.isActive());

    ResponseEntity<PriceDto> secondSaveResponseEntity = responseEntityProvider.apply(Boolean.FALSE);
    assertThat(secondSaveResponseEntity.getStatusCode(), equalTo(CREATED));
    PriceDto secondSavePrice = secondSaveResponseEntity.getBody();
    assertEquals(Boolean.FALSE, secondSavePrice.isActive());

    assertEquals(firstSavePrice.getId(), secondSavePrice.getId());

  }

  void assertActive(PriceDto priceDto) {
    priceRepository.findOneById(priceDto.getId())
        .ifPresentOrElse(
            savedPrice -> assertThat(savedPrice.isActive(), equalTo(true)),
            () -> Assertions.fail("Price should have beeen found"));
  }

  @Override
  DockerComposeContainer getEnvironment() {
    return environment;
  }
}
